package ep2;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

// Juntar: Delete, Add e Show;
// Delivery: for que cria um painel e adicona 3 labels: tipo, carga, distancia;
// Extrato: for que cria um painel e adiciona 3 labels pra esse paneil: tipo, custo, tempo
// Leitura/Escrita de arquivo;

public class GUI {
	
	private Program program = new Program();
	private DecimalFormat decimalFormat = new DecimalFormat("0.00");
	// JFrame
	private JFrame frame;
	// Container
	private Container container;
	// Panels
	private JPanel pnlCard;
	private JPanel pnlMain;
	private JPanel pnlLeft;
	private JPanel pnlInsert;
	private JPanel pnlShow;
	private JPanel pnlDelivery;
	private JPanel pnlExtract;
	
	private JPanel pnlOrder;
	private JPanel pnlData;
	private JPanel pnlLoad;
	private JPanel pnlDistance;
	private JPanel pnlTime;
	private JPanel pnlChoises;
	private JPanel pnlFaster;
	private JPanel pnlCheaper;
	private JPanel pnlBest;
	private JPanel pnlDataFaster;
	private JPanel pnlDataCheaper;
	private JPanel pnlDataBest;
	
	private JPanel pnlCar;
	private JPanel pnlMotorcycle;
	private JPanel pnlVan;
	private JPanel pnlTruck;
	
	private JPanel pnlFinalProfit;
	
	// Table
	private JTable tblDelivery;
	private JTable tblExtract;
	
	// Table Model
	private DefaultTableModel mdlDelivery;
	private DefaultTableModel mdlExtract;
	
	// Table Renderer
	private DefaultTableCellRenderer renderTable;
	
	// Labels
	private JLabel lblLogo;
	private JLabel lblDescription;
	private JLabel lblTruckImage;
	
	private JLabel lblLoad;
	private JLabel lblDistance;
	private JLabel lblTime;
	private JLabel lblTypeFaster;
	private JLabel lblFuelFaster;
	private JLabel lblTimeFaster;
	private JLabel lblCostFaster;
	private JLabel lblTypeCheaper;
	private JLabel lblFuelCheaper;
	private JLabel lblTimeCheaper;
	private JLabel lblCostCheaper;
	private JLabel lblTypeBest;
	private JLabel lblFuelBest;
	private JLabel lblTimeBest;
	private JLabel lblCostBest;
	private JLabel lblIconFaster;
	private JLabel lblIconCheaper;
	private JLabel lblIconBest;
	
	private JLabel lblCar;
	private JLabel lblCarTotal;
	private JLabel lblMotorcycle;
	private JLabel lblMotorcycleTotal;
	private JLabel lblVan;
	private JLabel lblVanTotal;
	private JLabel lblTruck;
	private JLabel lblTruckTotal;
	
	private JLabel lblTotal;
	private JLabel lblTotalValue;
	
	// Scroll Pane
	private JScrollPane scrDelivery;
	private JScrollPane scrExtract;
	
	// TextField
	private JTextField txtLoad;
	private JTextField txtDistance;
	private JTextField txtTime;

	// Icons
	private Icon iconInsert;
	private Icon iconShow;
	private Icon iconDelivery;
	private Icon iconExtract;
	private Icon iconProfit;
	
	// Buttons
	private JButton btnInsert;
	private JButton btnAdd;   // pnlInsert
	private JButton btnShow;
	private JButton btnDelivery;
	private JButton btnProfit;
	private JButton btnExtract;

	private JButton btnFaster;
	private JButton btnCheaper;
	private JButton btnBest;
	
	private JButton btnCarAdd;
	private JButton btnMotorcycleAdd;
	private JButton btnVanAdd;
	private JButton btnTruckAdd;
	private JButton btnCarRemove;
	private JButton btnMotorcycleRemove;
	private JButton btnVanRemove;
	private JButton btnTruckRemove;

	// Layout
	private CardLayout card;
	

	
	// ActionListener
	private ActionListener actSwitchPanels;
	private ActionListener actCreateVehicle;
	private ActionListener actRemoveVehicle;
	private ActionListener actNewDelivery;
	private ActionListener actSelectDelivery;
	
	private KeyAdapter keyOnlyNumbers;
	
	public GUI() {
		initComponents();
		addComponents();
		treatComponents();
		frame.setVisible(true);
	}
	public void initComponents() {

	// Layouts
	card = new CardLayout();
		
	// Frame
	frame = new JFrame();
	frame.setTitle("Vapt Vupt");
	frame.setSize(1200, 1000);
	frame.setLocationRelativeTo(null);
	frame.setIconImage((new ImageIcon("./icons/logo_frame.png")).getImage());
	frame.addWindowListener(new WindowAdapter() {
		
		public void windowClosing(WindowEvent e) {
			program.writeFiles(lblTotalValue.getText());
		}
		
	});
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
	
	// Container
	container = frame.getContentPane();
	container.setLayout(new BorderLayout());

	// Panels
	
	pnlCard = new JPanel();
	pnlCard.setLayout(card);
	
	pnlMain = new JPanel();
	pnlMain.setLayout(new GridLayout(3,1));
	pnlMain.setBackground(new Color(231,241,241));
	
	pnlLeft = new JPanel();
	pnlLeft.setLayout(new GridLayout(5,1,0,10));
	pnlLeft.setBackground(new Color(72,99,105));
	
	pnlInsert = new JPanel();
	pnlInsert.setLayout(new BorderLayout());
	pnlInsert.setBackground(new Color(231,241,241));
	
	pnlShow = new JPanel();
	pnlShow.setLayout(new GridLayout(4,1));
	pnlShow.setBackground(new Color(231,241,241));
	pnlShow.setBorder(new EmptyBorder(80,80,0,0));
	
	pnlDelivery = new JPanel();
	pnlDelivery.setLayout(new BorderLayout());
	pnlDelivery.setBackground(new Color(231,241,241));
	pnlDelivery.setBorder(new EmptyBorder(50,50,50,50));
	
	pnlExtract = new JPanel();
	pnlExtract.setLayout(new BorderLayout());
	pnlExtract.setBackground(new Color(231,241,241));
	pnlExtract.setBorder(new EmptyBorder(50,50,50,50));
	
	pnlOrder = new JPanel();
	pnlOrder.setLayout(new GridLayout(2,1));
	pnlOrder.setBackground(new Color(231,241,241));
	pnlOrder.setBorder(new EmptyBorder(70,70,20,20));
	
	pnlData = new JPanel();
	pnlData.setLayout(new GridLayout(3,1));
	pnlData.setBackground(new Color(231,241,241));

	pnlLoad = new JPanel();
	pnlLoad.setLayout(new FlowLayout());
	pnlLoad.setBackground(new Color(231,241,241));
	
	pnlDistance = new JPanel();
	pnlDistance.setLayout(new FlowLayout());
	pnlDistance.setBackground(new Color(231,241,241));
	
	pnlTime = new JPanel();
	pnlTime.setLayout(new FlowLayout());
	pnlTime.setBackground(new Color(231,241,241));

	pnlChoises = new JPanel();
	pnlChoises.setLayout(new GridLayout(3,1));
	pnlChoises.setBackground(new Color(231,241,241));
	
	pnlFaster = new JPanel();
	pnlFaster.setLayout(new GridLayout(1,3));
	pnlFaster.setBackground(new Color(231,241,241));
	
	pnlCheaper = new JPanel();
	pnlCheaper.setLayout(new GridLayout(1,3));
	pnlCheaper.setBackground(new Color(231,241,241));
	
	pnlBest = new JPanel();
	pnlBest.setLayout(new GridLayout(1,3));
	pnlBest.setBackground(new Color(231,241,241));
	
	pnlDataFaster = new JPanel();
	pnlDataFaster.setLayout(new GridLayout(4,1));
	pnlDataFaster.setBackground(new Color(231,241,241));
	
	pnlDataCheaper = new JPanel();
	pnlDataCheaper.setLayout(new GridLayout(4,1));
	pnlDataCheaper.setBackground(new Color(231,241,241));
	
	pnlDataBest = new JPanel();
	pnlDataBest.setLayout(new GridLayout(4,1));
	pnlDataBest.setBackground(new Color(231,241,241));
	
	pnlCar = new JPanel();
	pnlCar.setLayout(new FlowLayout(FlowLayout.LEFT));
	pnlCar.setBackground(new Color(231,241,241));
	
	pnlMotorcycle = new JPanel();
	pnlMotorcycle.setLayout(new FlowLayout(FlowLayout.LEFT));
	pnlMotorcycle.setBackground(new Color(231,241,241));

	pnlVan = new JPanel();
	pnlVan.setLayout(new FlowLayout(FlowLayout.LEFT));
	pnlVan.setBackground(new Color(231,241,241));

	pnlTruck = new JPanel();
	pnlTruck.setLayout(new FlowLayout(FlowLayout.LEFT));
	pnlTruck.setBackground(new Color(231,241,241));
	
	pnlFinalProfit = new JPanel();
	pnlFinalProfit.setLayout(new FlowLayout(FlowLayout.LEFT));
	pnlFinalProfit.setBackground(new Color(231,241,241));
	
	// Tables
	String[] delivery = {"Veículo", "Carga (kg)","Distancia (km)", "Tempo (hr)"};
	String[] extract = {"Veículo", "Data", "Valor"};
	
	renderTable = new DefaultTableCellRenderer();
	renderTable.setHorizontalAlignment( JLabel.CENTER );	
	
	mdlDelivery = new DefaultTableModel(0,0);
	mdlDelivery.setColumnIdentifiers(delivery);
	
	mdlExtract = new DefaultTableModel(0,0);
	mdlExtract.setColumnIdentifiers(extract);
	
	tblDelivery = new JTable();
	tblDelivery.setModel(mdlDelivery);
	tblDelivery.setBackground(new Color(247,255,255));
	tblDelivery.setRowHeight(40);
	tblDelivery.setFillsViewportHeight(true);
	tblDelivery.getColumnModel().getColumn(0).setCellRenderer(renderTable);
	tblDelivery.getColumnModel().getColumn(1).setCellRenderer(renderTable);
	tblDelivery.getColumnModel().getColumn(2).setCellRenderer(renderTable);
	tblDelivery.getColumnModel().getColumn(3).setCellRenderer(renderTable);
	
	tblExtract = new JTable();
	tblExtract.setModel(mdlExtract);
	tblExtract.setBackground(new Color(247,255,255));
	tblExtract.setRowHeight(40);
	tblExtract.setFillsViewportHeight(true);
	tblExtract.getColumnModel().getColumn(0).setCellRenderer(renderTable);
	tblExtract.getColumnModel().getColumn(1).setCellRenderer(renderTable);
	tblExtract.getColumnModel().getColumn(2).setCellRenderer(renderTable);
	
	// Scroll Pane
	scrDelivery = new JScrollPane(tblDelivery);
	scrExtract = new JScrollPane(tblExtract);

	// Labels
	lblLogo = new JLabel(new ImageIcon("./icons/logo.png"));
	lblDescription = new JLabel("Rapidez e Eficiência", JLabel.CENTER);
	lblDescription.setFont(new Font("Garamond",Font.BOLD,35));
	lblDescription.setForeground(new Color(72,99,105));
	lblTruckImage = new JLabel(new ImageIcon("./icons/truck-image.png"));
	
	lblLoad = new JLabel("        Carga: ", new ImageIcon("./icons/load.png"), JLabel.LEFT);
	lblDistance = new JLabel("        Distância: ", new ImageIcon("./icons/distance.png"), JLabel.LEFT);
	lblTime = new JLabel("        Tempo: ", new ImageIcon("./icons/time.png"), JLabel.LEFT);
	
	lblIconFaster = new JLabel(new ImageIcon("./icons/faster.png"));
	lblTypeFaster = new JLabel();
	lblFuelFaster = new JLabel();
	lblTimeFaster = new JLabel();
	lblCostFaster = new JLabel();
	
	lblIconCheaper = new JLabel(new ImageIcon("./icons/cheaper.png"));
	lblTypeCheaper = new JLabel();
	lblFuelCheaper = new JLabel();
	lblTimeCheaper = new JLabel();
	lblCostCheaper = new JLabel();
	
	lblIconBest = new JLabel(new ImageIcon("./icons/best.png"));
	lblTypeBest = new JLabel();
	lblFuelBest = new JLabel();
	lblTimeBest = new JLabel();
	lblCostBest = new JLabel();
	
	lblCar = new JLabel("           Carro:       ", new ImageIcon("./icons/car.png"), JLabel.LEFT);
	lblCarTotal = new JLabel(String.valueOf(program.getVehicle().size()));
	lblMotorcycle = new JLabel("           Moto:       ", new ImageIcon("./icons/motorcycle.png"), JLabel.LEFT);
	lblMotorcycleTotal = new JLabel(String.valueOf(program.getVehicle().size()));
	lblVan = new JLabel("           Van:       ", new ImageIcon("./icons/van.png"), JLabel.LEFT);
	lblVanTotal = new JLabel(String.valueOf(program.getVehicle().size()));
	lblTruck = new JLabel("           Caminhão:       ", new ImageIcon("./icons/pickup.png"), JLabel.LEFT);
	lblTruckTotal = new JLabel(String.valueOf(program.getVehicle().size()));
	
	lblTotal = new JLabel("      Total: ");
	lblTotalValue = new JLabel("0");
	
	// TextField
	txtLoad = new JTextField(10);
	txtDistance = new JTextField(10);
	txtTime = new JTextField(10);

	// Icons
	iconInsert = new ImageIcon("./icons/insert.png");
	iconShow = new ImageIcon("./icons/show.png");
	iconDelivery = new ImageIcon("./icons/delivery.png");
	iconExtract = new ImageIcon("./icons/extract.png");
	iconProfit = new ImageIcon("./icons/profit.png");
		
	// Buttons
	btnInsert = new JButton(iconInsert);
	btnShow = new JButton(iconShow);
	btnDelivery = new JButton(iconDelivery);
	btnExtract = new JButton(iconExtract);
	btnProfit = new JButton(iconProfit);
	btnAdd = new JButton("Inserir");
	
	btnCarAdd = new JButton("Criar", new ImageIcon("./icons/add.png"));
	btnCarAdd.setHorizontalAlignment(SwingConstants.LEFT);
	btnMotorcycleAdd = new JButton("Criar", new ImageIcon("./icons/add.png"));
	btnMotorcycleAdd.setHorizontalAlignment(SwingConstants.LEFT);
	btnVanAdd = new JButton("Criar", new ImageIcon("./icons/add.png"));
	btnVanAdd.setHorizontalAlignment(SwingConstants.LEFT);
	btnTruckAdd = new JButton("Criar", new ImageIcon("./icons/add.png"));
	btnTruckAdd.setHorizontalAlignment(SwingConstants.LEFT);
	
	btnCarRemove = new JButton("Remover", new ImageIcon("./icons/remove.png"));
	btnCarRemove.setHorizontalAlignment(SwingConstants.LEFT);
	btnMotorcycleRemove = new JButton("Remover", new ImageIcon("./icons/remove.png"));
	btnMotorcycleRemove.setHorizontalAlignment(SwingConstants.LEFT);
	btnVanRemove = new JButton("Remover", new ImageIcon("./icons/remove.png"));
	btnVanRemove.setHorizontalAlignment(SwingConstants.LEFT);
	btnTruckRemove = new JButton("Remover", new ImageIcon("./icons/remove.png"));
	btnTruckRemove.setHorizontalAlignment(SwingConstants.LEFT);
	
	btnFaster = new JButton("Pedido");
	btnCheaper = new JButton("Pedido");
	btnBest = new JButton("Pedido");
	// ActionListeners
	actSwitchPanels = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			String panel;
			
			JButton button = (JButton)e.getSource();
			
			pnlChoises.setVisible(false);
			txtLoad.setText("");
			txtDistance.setText("");
			txtTime.setText("");
			
			if(button == btnInsert)
				panel = "2";
			else if(button == btnShow) 
				panel = "3";
			else if(button == btnDelivery) {
				
				panel = "4";
				
					

			}
			else 
				panel = "5";
			
			card.show(pnlCard, panel);
			
		}
		
	};
	
	actCreateVehicle = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			JButton button = (JButton)e.getSource();

			
			if(button == btnCarAdd) {
				program.addVehicle("carro");
				lblCarTotal.setText(String.valueOf(program.getAmount("carro")));
			}
			else if(button == btnMotorcycleAdd) {
				program.addVehicle("moto");
				lblMotorcycleTotal.setText(String.valueOf(program.getAmount("moto")));
			}
			else if(button == btnVanAdd) {
				program.addVehicle("van");
				lblVanTotal.setText(String.valueOf(program.getAmount("van")));
			}
			else {
				program.addVehicle("caminhao");
				lblTruckTotal.setText(String.valueOf(program.getAmount("caminhao")));
			}

			
		}
		
	};
	
	actRemoveVehicle = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			JButton button = (JButton)e.getSource();
			
			
			if(button == btnCarRemove) {
				program.removeVehicle("carro");
				lblCarTotal.setText(String.valueOf(program.getAmount("carro")));
			}
			else if(button == btnMotorcycleRemove) {
				program.removeVehicle("moto");
				lblMotorcycleTotal.setText(String.valueOf(program.getAmount("moto")));
			}
			else if(button == btnVanRemove) {
				program.removeVehicle("van");
				lblVanTotal.setText(String.valueOf(program.getAmount("van")));
			}
			else {
				program.removeVehicle("caminhao");
				lblTruckTotal.setText(String.valueOf(program.getAmount("caminhao")));
			}
			
		}
		
	};
	
	actNewDelivery = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			String type;
			String fuel;
			
			Vehicle faster;
			Vehicle cheaper;
			Vehicle best;
			
			btnFaster.setEnabled(true);
			btnFaster.setContentAreaFilled(true);
			btnCheaper.setEnabled(true);
			btnCheaper.setContentAreaFilled(true);
			btnBest.setEnabled(true);
			btnBest.setContentAreaFilled(true);
			
	
			float load = Float.parseFloat(txtLoad.getText());
			float distance = Float.parseFloat(txtDistance.getText());
			float time = Float.parseFloat(txtTime.getText());
			
			type = program.fasterVehicle(load, distance, time);
			
			
			if(type != null) {
			fuel = program.bestFuel(type, distance, load);
			
			faster = program.createVehicle(type);
			faster.setFuel(program.createFuel(fuel));

			lblTypeFaster.setText("Tipo: " + faster.getType());
			lblFuelFaster.setText("Combustível: " + faster.getFuel().getType());
			lblTimeFaster.setText("Tempo: " + decimalFormat.format(distance/faster.getSpeed()));
			lblCostFaster.setText("Custo: " + decimalFormat.format(faster.costDelivery(fuel, load, distance, time)*100/(100-program.getProfit())));
			}
			else {
			lblTypeFaster.setText("");
			lblFuelFaster.setText("");
			lblTimeFaster.setText("Não há veículo capaz de realizar a entrega!");
			lblCostFaster.setText("");
			btnFaster.setEnabled(false);
			btnFaster.setContentAreaFilled(false);
			}

			type = program.cheaperVehicle(load, distance, time);
			

			if(type != null) {
			fuel = program.bestFuel(type, distance, load);

			cheaper = program.createVehicle(type);
			cheaper.setFuel(program.createFuel(fuel));
			
			lblTypeCheaper.setText("Tipo: " + cheaper.getType());
			lblFuelCheaper.setText("Combustível: " + cheaper.getFuel().getType());
			lblTimeCheaper.setText("Tempo: " + decimalFormat.format(distance/cheaper.getSpeed()));
			lblCostCheaper.setText("Custo: " + decimalFormat.format(cheaper.costDelivery(fuel, load, distance, time)*100/(100-program.getProfit())));
			}
			else {
			lblTypeCheaper.setText("");
			lblFuelCheaper.setText("");
			lblTimeCheaper.setText("Não há veículo capaz de realizar a entrega!");
			lblCostCheaper.setText("");
			btnCheaper.setEnabled(false);
			btnCheaper.setContentAreaFilled(false);
			}
			
			type = program.bestVehicle(distance, load, time);
			
			if(type != null) {
			fuel = program.bestFuel(type, distance, load);
			
			best = program.createVehicle(type);
			best.setFuel(program.createFuel(fuel));
			
			lblTypeBest.setText("Tipo: " + best.getType());
			lblFuelBest.setText("Combustível: " + best.getFuel().getType());
			lblTimeBest.setText("Tempo: " + decimalFormat.format(distance/best.getSpeed()));
			lblCostBest.setText("Custo: " + decimalFormat.format(best.costDelivery(fuel, load, distance, time)*100/(100-program.getProfit())));
			}else {
			lblTypeBest.setText("");
			lblFuelBest.setText("");
			lblTimeBest.setText("Não há veículo capaz de realizar a entrega!");
			lblCostBest.setText("");
			btnBest.setEnabled(false);
			btnBest.setContentAreaFilled(false);
			}
			
			pnlChoises.setVisible(true);
			
			
		}
		
	};
	
	actSelectDelivery = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {

			pnlChoises.setVisible(false);
			
			JButton button = (JButton)e.getSource();
			Vehicle vehicle;
			Delivery newDelivery;
			
			float load = Float.parseFloat(txtLoad.getText());
			float distance = Float.parseFloat(txtDistance.getText());
			int time = Integer.parseInt(txtTime.getText());
			
			String type;
			String fuelType;
			
			if(button == btnFaster) {
				type = program.fasterVehicle(distance, load, time);
				fuelType = program.bestFuel(type, distance, load);

				vehicle = program.createVehicle(type);
				
				newDelivery = program.newDelivery(type, fuelType, load, distance, time);
				
				if(newDelivery != null) {
				Object[] delivery = {vehicle.getType(), load, distance, distance/vehicle.getSpeed()};
				mdlDelivery.addRow(delivery);
				
				Object[] extract = {newDelivery.getVehicle().getType(),  new SimpleDateFormat("dd/MM").format(new Date()), decimalFormat.format(newDelivery.getCost()*100/(100-program.getProfit()))};
				mdlExtract.addRow(extract);
				}
			}
				
			else if(button == btnCheaper) {
				type = program.cheaperVehicle(distance, load, time);
				fuelType = program.bestFuel(type, distance, load);
				
				vehicle = program.createVehicle(type);
				
				newDelivery = program.newDelivery(type, fuelType, load, distance, time);
				
				if(newDelivery != null) {
				Object[] delivery = {vehicle.getType(), load, distance, distance/vehicle.getSpeed()};
				mdlDelivery.addRow(delivery);
				
				Object[] extract = {newDelivery.getVehicle().getType(), new SimpleDateFormat("dd/MM").format(new Date()), decimalFormat.format(newDelivery.getCost()*100/(100-program.getProfit()))};
				mdlExtract.addRow(extract);
				}
			}
				
			else {
				type = program.bestVehicle(distance, load, time);
				fuelType = program.bestFuel(type, distance, load);

				vehicle = program.createVehicle(type);

				newDelivery = program.newDelivery(type, fuelType, load, distance, time);
				
				if(newDelivery != null) {
				Object[] delivery = {vehicle.getType(), load, distance, decimalFormat.format(distance/vehicle.getSpeed())};
				mdlDelivery.addRow(delivery);
				
				Object[] extract = {newDelivery.getVehicle().getType(), new SimpleDateFormat("dd/MM").format(new Date()), decimalFormat.format(newDelivery.getCost()*100/(100-program.getProfit()))};
				mdlExtract.addRow(extract);
				}
				
			}
			
			if(newDelivery == null) {
				JOptionPane.showMessageDialog(null, "Não há veículo disponível para a entrega!");
			
			}else {
				JOptionPane.showMessageDialog(null, "Entrega registrada com sucesso!");
				float profit = Float.parseFloat(lblTotalValue.getText());
				profit = (float) (profit + newDelivery.getCost()*100/(100-program.getProfit()));
				lblTotalValue.setText(String.valueOf(decimalFormat.format(profit)));
			}
			txtLoad.setText("");
			txtDistance.setText("");
			txtTime.setText("");
		}
		
	};
	
		keyOnlyNumbers = new KeyAdapter() {

				
			@Override
			public void keyTyped(KeyEvent e) {
				
				char key = e.getKeyChar();
				
				if(!((key >= '0') && (key <= '9') || (key == KeyEvent.VK_BACK_SPACE) ||(key == KeyEvent.VK_DELETE) || (key == '.'))) {
					e.consume();
				}
				
						
			}
		
	};
	
	}
	
	public void addComponents() {
		
		// Panels
		container.add(pnlCard, BorderLayout.CENTER);
		container.add(pnlLeft, BorderLayout.WEST);
		
		pnlInsert.add(pnlOrder, BorderLayout.CENTER);
		
		pnlFaster.add(lblIconFaster);
		pnlFaster.add(pnlDataFaster);
		pnlFaster.add(btnFaster);

		pnlCheaper.add(lblIconCheaper);
		pnlCheaper.add(pnlDataCheaper);
		pnlCheaper.add(btnCheaper);
		
		pnlBest.add(lblIconBest);
		pnlBest.add(pnlDataBest);
		pnlBest.add(btnBest);
		
		pnlData.add(pnlLoad);
		pnlData.add(pnlDistance);
		pnlData.add(pnlTime);
		
		pnlChoises.add(pnlFaster);
		pnlChoises.add(pnlCheaper);
		pnlChoises.add(pnlBest);

		pnlOrder.add(pnlData);
		pnlOrder.add(pnlChoises);
		
		pnlShow.add(pnlCar);
		pnlShow.add(pnlMotorcycle);
		pnlShow.add(pnlVan);
		pnlShow.add(pnlTruck);
		
		pnlExtract.add(pnlFinalProfit, BorderLayout.SOUTH);
	
		// Scroll Pane
		pnlDelivery.add(scrDelivery, BorderLayout.CENTER);
		pnlExtract.add(scrExtract, BorderLayout.CENTER);
		
		// CardLayout
		
		pnlCard.add(pnlMain, "1");
		pnlCard.add(pnlInsert, "2");
		pnlCard.add(pnlShow, "3");
		pnlCard.add(pnlDelivery, "4");
		pnlCard.add(pnlExtract, "5");
		
		card.show(pnlCard, "1");
		
		// Labels
		pnlMain.add(lblLogo);
		pnlMain.add(lblDescription);
		pnlMain.add(lblTruckImage);
		
		pnlLoad.add(lblLoad);
		pnlDistance.add(lblDistance);
		pnlTime.add(lblTime);
		
		pnlDataFaster.add(lblTypeFaster);
		pnlDataFaster.add(lblFuelFaster);
		pnlDataFaster.add(lblTimeFaster);
		pnlDataFaster.add(lblCostFaster);
		
		pnlDataCheaper.add(lblTypeCheaper);
		pnlDataCheaper.add(lblFuelCheaper);
		pnlDataCheaper.add(lblTimeCheaper);
		pnlDataCheaper.add(lblCostCheaper);

		pnlDataBest.add(lblTypeBest);
		pnlDataBest.add(lblFuelBest);
		pnlDataBest.add(lblTimeBest);
		pnlDataBest.add(lblCostBest);
		
		pnlCar.add(lblCar);
		pnlCar.add(lblCarTotal);
		pnlCar.add(new JLabel("          "));
		pnlMotorcycle.add(lblMotorcycle);
		pnlMotorcycle.add(lblMotorcycleTotal);
		pnlMotorcycle.add(new JLabel("          "));
		pnlVan.add(lblVan);
		pnlVan.add(lblVanTotal);
		pnlVan.add(new JLabel("          "));
		pnlTruck.add(lblTruck);
		pnlTruck.add(lblTruckTotal);
		pnlTruck.add(new JLabel("          "));
		
		pnlFinalProfit.add(lblTotal);
		pnlFinalProfit.add(lblTotalValue);
		
		// TextFields
		pnlLoad.add(txtLoad);
		pnlDistance.add(txtDistance);
		pnlTime.add(txtTime);
		
		// Buttons
		pnlLeft.add(btnInsert);
		pnlLeft.add(btnShow);
		pnlLeft.add(btnDelivery);
		pnlLeft.add(btnExtract);
		pnlLeft.add(btnProfit);
		
		pnlInsert.add(btnAdd, BorderLayout.SOUTH);

		
		pnlCar.add(btnCarAdd);
		pnlMotorcycle.add(btnMotorcycleAdd);
		pnlVan.add(btnVanAdd);
		pnlTruck.add(btnTruckAdd);
		
		pnlCar.add(btnCarRemove);
		pnlMotorcycle.add(btnMotorcycleRemove);
		pnlVan.add(btnVanRemove);
		pnlTruck.add(btnTruckRemove);
		
		// ActionListeners
		txtLoad.addKeyListener(keyOnlyNumbers);
		txtDistance.addKeyListener(keyOnlyNumbers);
		txtTime.addKeyListener(keyOnlyNumbers);
		
		btnInsert.addActionListener(actSwitchPanels);
		btnShow.addActionListener(actSwitchPanels);
		btnDelivery.addActionListener(actSwitchPanels);
		btnExtract.addActionListener(actSwitchPanels);
		
		btnCarAdd.addActionListener(actCreateVehicle);
		btnMotorcycleAdd.addActionListener(actCreateVehicle);
		btnVanAdd.addActionListener(actCreateVehicle);
		btnTruckAdd.addActionListener(actCreateVehicle);

		btnCarRemove.addActionListener(actRemoveVehicle);
		btnMotorcycleRemove.addActionListener(actRemoveVehicle);
		btnVanRemove.addActionListener(actRemoveVehicle);
		btnTruckRemove.addActionListener(actRemoveVehicle);
		
		btnAdd.addActionListener(actNewDelivery);
		
		btnFaster.addActionListener(actSelectDelivery);
		btnCheaper.addActionListener(actSelectDelivery);
		btnBest.addActionListener(actSelectDelivery);
		
		btnProfit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String strProfit = null;
				
				try {
					strProfit = (String) JOptionPane.showInputDialog(null,"Percentual de Lucro","Lucro", 0, new ImageIcon("./icons/cash.png"), null, null);
				}catch(NumberFormatException e) {
					
				}catch(Exception e) {
					
				}
				

					if(strProfit != null && !strProfit.isEmpty() && strProfit.toUpperCase() == strProfit.toLowerCase()) {
						if(Float.parseFloat(strProfit) < 100 && Float.parseFloat(strProfit) >= 0)
							program.setProfit(Float.parseFloat(strProfit));
					}
				
			}
			
		});

	}
	
	public void treatComponents() {
		treatButton(btnInsert);
		treatButton(btnShow);
		treatButton(btnDelivery);
		treatButton(btnExtract);
		treatButton(btnProfit);
	}
	
	public void treatButton(JButton btnButton) {
		
		btnButton.setBorderPainted(false);
		btnButton.setFocusPainted(false);
		btnButton.setContentAreaFilled(false);	
	}

	public void createFromFile() {
		float totalProfit = program.readFiles();
		lblTotalValue.setText(String.valueOf(totalProfit));
		
		String type;
		float load;
		float distance;
		float time;
		double cost;
		
		for(Delivery d : program.getExtract()) {
			
			type = d.getVehicle().getType();

			load = d.getLoad();
			
			distance = d.getDistance();
			
			time = d.getTime();
			
			cost = d.getCost();
			
			Object[] delivery = {type, load, distance, decimalFormat.format(time)};
			mdlDelivery.addRow(delivery);
			
			Object[] extract = {type, d.getDate(), decimalFormat.format(cost*100/(100-program.getProfit()))};
			mdlExtract.addRow(extract);
		}
		
		lblCarTotal.setText(String.valueOf(program.getAmount("carro")));
		lblMotorcycleTotal.setText(String.valueOf(program.getAmount("moto")));
		lblVanTotal.setText(String.valueOf(program.getAmount("van")));
		lblTruckTotal.setText(String.valueOf(program.getAmount("caminhao")));
			
		
	}
	public static void main(String[] args) {
		
		GUI gui = new GUI();
		gui.createFromFile();

	}

}
