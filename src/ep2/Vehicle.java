package ep2;

public class Vehicle {
	
	private String type;
	private double yield;
	private double yield_coef;
	private float max_load;
	private float load;
	private int speed;
	private Fuel fuel;
	private int status;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getYield() {
		return yield;
	}

	public void setYield(double yield) {
		this.yield = yield;
	}

	public double getYield_coef() {
		return yield_coef;
	}

	public void setYield_coef(double yield_coef) {
		this.yield_coef = yield_coef;
	}

	public float getMax_load() {
		return max_load;
	}

	public void setMax_load(float max_load) {
		this.max_load = max_load;
	}

	public float getLoad() {
		return load;
	}

	public void setLoad(float load) {
		this.load = load;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public Fuel getFuel() {
		return fuel;
	}

	public void setFuel(Fuel fuel) {
		this.fuel = fuel;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public boolean canDelivery(float load, float distance, float time) {
		if(load <= getMax_load() && time >= (distance/getSpeed()))		
			return true;
		else
			return false;
	}
	
	public double costDelivery(String fuel, float load, float distance, float time) {
		
		float fuelPrice;
		
		if(fuel.equalsIgnoreCase("gasolina"))
			fuelPrice = Program.GASOLINE;
		else if(fuel.contentEquals("alcool"))
			fuelPrice = Program.ALCOHOL;
		else
			fuelPrice = Program.DIESEL;
			
		return ((distance)/newYield(load)) * fuelPrice;
		
	}
	
	public double newYield(float load, String fuelType) {
		 return( yield - (load * yield_coef));
	}

	public double newYield(float load) {

		 return( getYield() - (load * getYield_coef()));
	}
	
}
