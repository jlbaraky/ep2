package ep2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

public class Program {
	
	static final int  FREE = 0;
	static final int  BUSY = 1;
	static final float  GASOLINE = 4.449f;
	static final float  ALCOHOL = 3.499f;
	static final float  DIESEL = 3.869f;
	
	private float profit;
	private ArrayList<Vehicle> listVehicle;
	private ArrayList<Vehicle> listDelivery;
	private ArrayList<Delivery> listExtract;

	
	
	Program() {
		profit = 20.0f;
		listVehicle = new ArrayList<Vehicle>();
		listDelivery = new ArrayList<Vehicle>();
		listExtract = new ArrayList<Delivery>();
	}
	
	public void addVehicle(String type) {
		
			
		if(type.equalsIgnoreCase("caminhao"))
			listVehicle.add(new Truck());
		else if(type.equalsIgnoreCase("carro"))
			listVehicle.add(new Car());
		else if(type.equalsIgnoreCase("moto"))
			listVehicle.add(new Motorcycle());
		else
			listVehicle.add(new Van());
		
	}
	
	public Delivery newDelivery(String type, String fuelType, float load, float distance, float time) {
		
		Delivery delivery = null;
		Fuel fuel;
		
		fuelType = fuelType.toLowerCase();
		type = type.toLowerCase();
		
		if(fuelType == "gasolina")
			fuel = new Gasoline();
		else if (fuelType == "diesel")
			fuel = new Diesel();
		else
			fuel = new Alcohol();
		
		for(Vehicle v : listVehicle) {
			
			
			if(type.equalsIgnoreCase(v.getType()) && v.getStatus() == FREE) {
				v.setStatus(BUSY);
				v.setFuel(fuel);
				v.setLoad(load);
				listDelivery.add(v);
				
				delivery = new Delivery(v, fuel, load, distance, time);
				listExtract.add(delivery);
				
				break;
			}
			
		}
		
		return delivery;
		
	}
	
	public void removeVehicle(String type) {
		
		
		
		for(Vehicle v : listVehicle) {
			if(type.equalsIgnoreCase(v.getType()) && v.getStatus() == FREE) {
				listVehicle.remove(v);
				break;
			}
		}
				
		
	}
	
	public ArrayList<Vehicle> getVehicle() {
		
		return listVehicle;
	}
	
	public ArrayList<Vehicle> getDelivery() {
		
		return listDelivery;
	}
	
	public ArrayList<Delivery> getExtract() {
		
		return listExtract;
	}
	
	public void setProfit(float profit) {
		this.profit = profit;
	}
	
	public float getProfit() {
		return profit;
	}
	
	
	////////////////////////////
	
	public void readVehicle() {
		
	}
	
	public String fasterVehicle(Float load, float distance, float time) {
		
		int car = 0;
		int motorcycle = 0;
		int van = 0;
		int truck = 0;
		
		for(Vehicle v : listVehicle) {
			if(v.getType().equalsIgnoreCase("carro") && v.getStatus() == FREE)
				car++;
			if(v.getType().equalsIgnoreCase("moto") && v.getStatus() == FREE)
				motorcycle++;
			if(v.getType().equalsIgnoreCase("van") && v.getStatus() == FREE)
				van++;
			if(v.getType().equalsIgnoreCase("caminhao") && v.getStatus() == FREE)
				truck++;
		}
		
		if(load <= 50 && time >= (distance / 110) && motorcycle != 0)
			return "moto";
		else if(load <= 360 && time >= (distance / 100) && car != 0)
			return "carro";
		else if(load <= 3500 && time >= (distance / 80) && van != 0)
			return "van";
		else if(load <= 30000 && time >= (distance / 60) && truck != 0)
			return "caminhao";
		else
			return null;
		
	}
	
	
	public String cheaperVehicle(Float load, Float distance, float time) {
		
		double cost1 = 0;
		double cost2 = 0;
		double cost3 = 0;
		double cost4 = 0;
	
		ArrayList<Double> cost = new ArrayList<Double>();
		
		int car = 0;
		int motorcycle = 0;
		int van = 0;
		int truck = 0;
		
		for(Vehicle v : listVehicle) {
			if(v.getType().equalsIgnoreCase("carro")&& v.getStatus() == FREE)
				car++;
			if(v.getType().equalsIgnoreCase("moto")&& v.getStatus() == FREE)
				motorcycle++;
			if(v.getType().equalsIgnoreCase("van")&& v.getStatus() == FREE)
				van++;
			if(v.getType().equalsIgnoreCase("caminhao")&& v.getStatus() == FREE)
				truck++;
		}

		if((new Motorcycle()).canDelivery(load, distance, time) && motorcycle != 0) {
			if(bestFuel("moto", distance, load).equalsIgnoreCase("gasolina"))
				cost1 = (new Motorcycle()).costDelivery("gasolina", load, distance, time);
			else
				cost1 = (new Motorcycle()).costDelivery("alcool", load, distance, time);
		}
		if((new Car()).canDelivery(load, distance, time) && car != 0) {
			if(bestFuel("carro", distance, load).equalsIgnoreCase("gasolina"))
				cost2 = (new Car()).costDelivery("gasolina", load, distance, time);
			else
				cost2 = (new Car()).costDelivery("alcool", load, distance, time);
			
		}

		if((new Van()).canDelivery(load, distance, time) && van != 0) {
			cost3 = (new Van()).costDelivery("diesel", load, distance, time);
		}
		if((new Truck()).canDelivery(load, distance, time) && truck != 0) 
			cost4 = (new Truck()).costDelivery("diesel", load, distance, time);

		
		
		if(cost1 != 0) {
			cost.add(cost1);
		}
		if(cost2 != 0) {
			cost.add(cost2);
		}
		if(cost3 != 0) {
			cost.add(cost3);
		}
		if(cost4 != 0) {
			cost.add(cost4);
		}
		
		cost.sort(null);
		
		if(cost.size() == 0)
			return null;
		
		if(cost.get(0) == cost1)
			return "moto";
		else if(cost.get(0) == cost2)
			return "carro";
		else if(cost.get(0) == cost3)
			return "van";
		else
			return "caminhao";
		

	}
	
	public String bestVehicle(float distance, float load, float time) {
		
		double cost1 = 0;
		double cost2 = 0;
		double cost3 = 0;
		double cost4 = 0;
	
		ArrayList<Double> cost = new ArrayList<Double>();
		
		int car = 0;
		int motorcycle = 0;
		int van = 0;
		int truck = 0;
		
		for(Vehicle v : listVehicle) {
			if(v.getType().equalsIgnoreCase("carro") && v.getStatus() == FREE)
				car++;
			if(v.getType().equalsIgnoreCase("moto") && v.getStatus() == FREE)
				motorcycle++;
			if(v.getType().equalsIgnoreCase("van") && v.getStatus() == FREE)
				van++;
			if(v.getType().equalsIgnoreCase("caminhao") && v.getStatus() == FREE)
				truck++;
		}

		if((new Motorcycle()).canDelivery(load, distance, time) && motorcycle != 0) {
			if(bestFuel("moto", distance, load).equalsIgnoreCase("gasolina")) {
				cost1 = 110/(new Motorcycle()).costDelivery("gasolina", load, distance, time);
			}
			else
				cost1 = 110/(new Motorcycle()).costDelivery("alcool", load, distance, time);
		}
		if((new Car()).canDelivery(load, distance, time) && car != 0) {
			if(bestFuel("carro", distance, load).equalsIgnoreCase("gasolina"))
				cost2 = 100/(new Car()).costDelivery("gasolina", load, distance, time);
			else
				cost2 = 100/(new Car()).costDelivery("alcool", load, distance, time);
			
		}

		if((new Van()).canDelivery(load, distance, time) && van != 0) {
			cost3 = 80/(new Van()).costDelivery("diesel", load, distance, time);
		}
		if((new Truck()).canDelivery(load, distance, time) && truck != 0) 
			cost4 = 60/(new Truck()).costDelivery("diesel", load, distance, time);
		
		
		if(cost1 != 0) {
			cost.add(-1*cost1);
		}
		if(cost2 != 0) {
			cost.add(-1*cost2);
		}
		if(cost3 != 0) {
			cost.add(-1*cost3);
		}
		if(cost4 != 0) {
			cost.add(-1*cost4);
		}
		
		cost.sort(null);
		
		if(cost.size() == 0)
			return null;
		
		if(cost.get(0) == -1*cost1)
			return "moto";
		else if(cost.get(0) == -1*cost2)
			return "carro";
		else if(cost.get(0) == -1*cost3)
			return "van";
		else 
			return "caminhao";
		
		
		
		
		
	}
	
	public String bestFuel(String type, float distance, float load) {
		
		double cost1;
		double cost2;
	
		if(type.equalsIgnoreCase("carro") || type.equalsIgnoreCase("moto")) {
			
			if(type.equalsIgnoreCase("carro" )) {
			
				cost1 = ((distance)/(new Car()).newYield(load, "gasolina")) * GASOLINE;
				cost2 = ((distance)/(new Car()).newYield(load, "alcool")) * ALCOHOL;
			}
			else {
			
				cost1 = ((distance)/(new Motorcycle()).newYield(load, "gasolina")) * GASOLINE;
				cost2 = ((distance)/(new Motorcycle()).newYield(load, "alcool")) * ALCOHOL;
			}

				if(cost1 < cost2)
					return "gasolina";
				else
					return "alcool";
			
		}
		else {

			return "diesel";
			
		}
		
		
	}
	
	
	public Vehicle createVehicle(String type) {
		
		if(type.equalsIgnoreCase("carro")) {
			return (new Car());
		}
		else if(type.equalsIgnoreCase("moto")) {
			return (new Motorcycle());
		}
		else if(type.equalsIgnoreCase("van")) {
			return (new Van());
		}
		else {
			return (new Truck());
		}
	}
	
	public Fuel createFuel(String type) {
		
		if(type.equalsIgnoreCase("gasolina")) {
			return (new Gasoline());
		}
		else if(type.equalsIgnoreCase("alcool")) {
			return (new Alcohol());
		}
		else  {
			return (new Diesel());
		}
	}
	
	public int getAmount(String type) {
		
		int i = 0;
		
		for(Vehicle v : listVehicle) {
		
			if(type.equalsIgnoreCase(v.getType()))
				i++;
		}
		
		return i;
	}
	
	public float readFiles() {

		float totalProfit = 0.0f;
		try {
			BufferedReader vehicle = new BufferedReader(new FileReader("./documents/vehicle.txt"));
			String line;
			String[] names = {"carro", "moto", "van", "caminhao"};
			
			int j = 0;
			while((line = vehicle.readLine()) != null) {
				for(int i=0; i<Integer.parseInt(line);i++){
					
					
					addVehicle(names[j]);
					
				}
				j++;
			}
			
			vehicle.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			BufferedReader extract = new BufferedReader(new FileReader("./documents/extract.txt"));
		
			String line = null;
			String newLine;
			String type;
			String fuel;
			float load;
			float distance;
			float time;
			String date;
			
			int i = 0;
			int j = 0;
			
			line = extract.readLine();
			
			if(line != null && !line.isEmpty())
				setProfit(Float.parseFloat(line));
			
			line = extract.readLine();
			
			if(line != null && !line.isEmpty())
				totalProfit = Float.parseFloat(line);
			
			
			while((line = extract.readLine()) != null) {
				
				i = 0;
				
				j = line.indexOf('$');
				type = line.substring(i,j);
				i = j+1;

				newLine = line.substring(i, line.length());
				j = newLine.indexOf('$');
				load = Float.parseFloat(newLine.substring(0,j));
				i = j+1;
				
				newLine = newLine.substring(i, newLine.length());
				j = newLine.indexOf('$');
				distance = Float.parseFloat(newLine.substring(0,j));
				i = j+1;
				
				newLine = newLine.substring(i, newLine.length());
				j = newLine.indexOf('$');
				time = Float.parseFloat(newLine.substring(0,j));
				i = j+1;
				
				newLine = newLine.substring(i, newLine.length());
				date = newLine.substring(0,newLine.length());
				
				fuel = bestFuel(type, distance, load);
				
				Fuel f = createFuel(fuel);
				Vehicle v = createVehicle(type);
				
				
				Delivery d = new Delivery(v, f, load, distance, time);

				d.setDate(date);

				listExtract.add(d);
				
			}
			
			extract.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return totalProfit;
	}
	
	public void writeFiles(String finalProfit) {
		
		try {
			Writer vehicle = new FileWriter("./documents/vehicle.txt");
			
			
			int car = 0;
			int motorcycle = 0;
			int van = 0;
			int truck = 0;

			for(Vehicle v : listVehicle) {
				
				if(v.getType().equalsIgnoreCase("carro")) {
					car++;
				}
				else if(v.getType().equalsIgnoreCase("moto")) {
					motorcycle++;
				}
				else if(v.getType().equalsIgnoreCase("van")) {
					van++;
				}
				else {
					truck++;
				}
			}
			
				

				vehicle.write(String.valueOf(car) + "\n");
				vehicle.write(String.valueOf(motorcycle) + "\n");
				vehicle.write(String.valueOf(van) + "\n");
				vehicle.write(String.valueOf(truck) + "\n");
			
				vehicle.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Writer extract = new FileWriter("./documents/extract.txt");
			
			
			extract.write(String.valueOf(getProfit()) + "\n");
			extract.write(finalProfit + "\n");
			
			String type;
			String load;
			String distance;
			String time;
			String date;
			
			for(Delivery d : listExtract) {
				
				type = d.getVehicle().getType().toLowerCase();
				load = String.valueOf(d.getLoad());
				distance = String.valueOf(d.getDistance());
				time = String.valueOf(d.getDistance()/d.getVehicle().getSpeed());
				date = d.getDate();				
				
				extract.write(type);
				extract.write('$');
				extract.write(load);
				extract.write('$');
				extract.write(distance);
				extract.write('$');
				extract.write(time);
				extract.write('$');
				extract.write(date + "\n");

			}
			
			extract.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
// Duplicar o valor ida e volta
	
//	O sistema deve persistir (armazenar) a frota de veículos da empresa, possibilitando a criação e a remoção de veículos existentes;
//	O sistema deve permitir que o usuário possa configurar a margem de lucro com a qual os cálculos serão realizados;
//	O sistema deve persistir (armazenar) a margem de lucro inserida pelo usuário;
//	O sistema deve receber, do usuário, os seguintes dados no momento da realização dos cálculos:
//	 -	O peso da carga a ser entregue;
//	 -	A distância a ser percorrida na entrega;
//	 -	O tempo máximo no qual a entrega deve ser realizada;
	
//	O sistema deve mostrar, ao usuário, os seguintes dados após a realização dos cálculos:
//	 -	O veículo disponível que possui o menor custo de operação para a carga dada, o tempo que levará para ele realizar a entrega e o custo de operação somado com a margem de lucro;
//	 -	O veículo disponível que mais rápido consegue realizar a entrega, o tempo que levará para ele realizar a entrega e o custo de operação somado com a margem de lucro;
//	 -	O veículo disponível que possui o melhor custo benefício para a empresa, o tempo que levará para ele realizar a entrega e o custo de operação somado com a margem de lucro;
//	 -	Caso exista veículo que consiga realizar a entrega sem exceder os limites de peso e de tempo, deve ser possível selecioná-lo para o serviço;
//	 -	Caso não exista veículo que consiga realizar a entrega sem exceder os limites de peso e de tempo, o usuário deve ser alertado e não pode ser possível selecionar veículos para o serviço;
//	 -	Os veículos selecionados para serviço não podem ser utilizados em outros fretes sem que o usuário os torne disponível novamente;
//	 -	A carga deve ser entregue em apenas uma viagem, por apenas um veículo;	
	
}
