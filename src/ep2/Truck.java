package ep2;

public class Truck extends Vehicle{
	
	Truck(){
		
		setType("Caminhao");
		setYield(8);
		setYield_coef(0.0002);
		setMax_load(30000f);
		setLoad(0f);
		setSpeed(60);
		setFuel(null);
		setStatus(Program.FREE);
		
		
	}
}
