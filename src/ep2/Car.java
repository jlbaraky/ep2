package ep2;

public class Car extends Vehicle {
	
	static final float YIELD_GASOLINE = 14.0f;
	static final float YIELD_ALCOHOL = 12.0f;
	static final float YIELD_GASOLINE_COEF = 0.025f;
	static final float YIELD_ALCOHOL_COEF = 0.0231f;
	
	Car(){
		
		setType("Carro");
		setYield(14);
		setYield_coef(0.025);
		setMax_load(360f);
		setLoad(0f);
		setSpeed(100);
		setFuel(null);
		setStatus(Program.FREE);
		
		
		
	}
	
	public double newYield(float load, String fuelType) {
		
		
		if(fuelType.equalsIgnoreCase("gasolina")) {
			setYield_coef(YIELD_GASOLINE_COEF);
			setYield(YIELD_GASOLINE);
			
		}
		else {
			setYield_coef(YIELD_ALCOHOL_COEF);
			setYield(YIELD_ALCOHOL);
			
		}
		
		return(getYield() - (load * getYield_coef()));
	}

	public double costDelivery(String fuel, float load, float distance, float time) {
		
		float fuelPrice;
		
		if(fuel.equalsIgnoreCase("gasolina"))
			fuelPrice = Program.GASOLINE;
		else if(fuel.equalsIgnoreCase("alcool"))
			fuelPrice = Program.ALCOHOL;
		else
			fuelPrice = Program.DIESEL;
			
		return ((distance)/newYield(load, fuel)) * fuelPrice;
		
	}
}
