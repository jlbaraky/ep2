package ep2;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Delivery {
	
	private Vehicle vehicle;
	private Fuel fuel;
	private float load;
	private float distance;
	private float time;
	private double cost;
	private String date;
	
	
	
	public Delivery(Vehicle vehicle, Fuel fuel, float load, float distance, float time) {
		this.vehicle = vehicle;
		this.fuel = fuel;
		this.load = load;
		this.distance = distance;
		this.time = time;
		this.cost = ((distance)/vehicle.newYield(load, fuel.getType())) * fuel.getCost();
		this.date = new SimpleDateFormat("dd/MM").format(new Date());
	}

	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Fuel getFuel() {
		return fuel;
	}
	public void setFuel(Fuel fuel) {
		this.fuel = fuel;
	}
	public float getLoad() {
		return load;
	}
	public void setLoad(float load) {
		this.load = load;
	}
	public float getDistance() {
		return distance;
	}
	public void setDistance(float distance) {
		this.distance = distance;
	}
	public float getTime() {
		return time;
	}
	public void setTime(float time) {
		this.time = time;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	
	

}
