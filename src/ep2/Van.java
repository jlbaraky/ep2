package ep2;

public class Van extends Vehicle {

	Van(){
		
		setType("Van");
		setYield(10);
		setYield_coef(0.001);
		setMax_load(3500f);
		setLoad(0f);
		setSpeed(80);
		setFuel(null);
		setStatus(Program.FREE);
		
		
	}
}
