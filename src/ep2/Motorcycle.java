package ep2;

public class Motorcycle extends Vehicle {
	
	static final float YIELD_GASOLINE = 50.0f;
	static final float YIELD_ALCOHOL = 43.0f;
	static final float YIELD_GASOLINE_COEF = 0.3f;
	static final float YIELD_ALCOHOL_COEF = 0.4f;

	Motorcycle(){
		
		setType("Moto");
		setYield_coef(50);
		setYield_coef(0.3);
		setMax_load(50f);
		setLoad(0f);
		setSpeed(110);
		setFuel(null);
		setStatus(Program.FREE);
		
		
	}

	public double newYield(float load, String fuelType) {
		
		
		if(fuelType.equalsIgnoreCase("gasolina")) {
			setYield_coef(YIELD_GASOLINE_COEF);
			setYield(YIELD_GASOLINE);
		}
		else {
			setYield_coef(YIELD_ALCOHOL_COEF);
			setYield(YIELD_ALCOHOL);
		}
		
		return(getYield() - (load * getYield_coef()));
	}

	public double costDelivery(String fuel, float load, float distance, float time) {
		
		float fuelPrice;
		
		if(fuel == "gasolina")
			fuelPrice = Program.GASOLINE;
		else if(fuel == "alcool")
			fuelPrice = Program.ALCOHOL;
		else
			fuelPrice = Program.DIESEL;
			
		return ((distance)/newYield(load, fuel)) * fuelPrice;
		
	}
}
