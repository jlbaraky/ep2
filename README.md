# Vapt Vupt - EP2 OO 2019

**Vapt Vupt** é uma aplicação desktop que permite o gerenciamento de uma transportadora de maneira rápida e intuitiva. Possuindo as seguintes características:
- Controle de frotas. Podendo adicionar e/ou remover veículos para espelhar a realidade;
- Inserir um pedido de transporte no sistema;
- Analizar o histórico de fretes feitos;
- Acesso ao 'extrato bancário' das entregas realizadas;
- Controle do lucro desejado.

## Benefícios

A funcionalidade que destaca Vapt Vupt das demais aplicações é seu sistema inteligente que fornece ao usuário 3 formas de frete:

- O mais **veloz**. Perfeito para o cliente que precisa da encomenda o mais rápido possível; 
- O mais **barato**. Économico, perfeito para quando o cliente não necessita do produto urgentemente e o orçamento é baixo;
- O melhor **custo-benefício**. Ótimo para uma boa primeira impressão.

E sem falar do design moderno, que oferece mais comodidade ao usuário. Cá entre nós, já chega desses programas com desing de software de padaria, né?

## Instruções

Para usar o Vapt Vupt é muito simples. Ao abrir o aplicativo você irá direto para essa tela:

<p align="center"> 
<img src="https://i.imgur.com/reDU8Wd.jpg">
</p>

No lado esquerdo há uma série de botões que possui funções diferentes.

O primeiro botão é para fazer o pedido:

<p align="center"> 
<img src="https://i.imgur.com/1X62Ppj.jpg">
</p>

Ao clicar nele você irá para essa tela onde fornecerá os dados necessários para o cálculo dos 3 tipos de frete:

<p align="center"> 
<img src="https://i.imgur.com/5dhsbxx.jpg">
</p>

Ao clicar no botão "Inserir" lá embaixo, irá aparecer essas opções a qual você poderá escolher uma:

<p align="center"> 
<img src="https://i.imgur.com/rlZAz1z.jpg">
</p>

O segundo botão é o controle das frotas:

<p align="center"> 
<img src="https://i.imgur.com/G12XfIO.jpg">
</p>

Nesta tela você podera adicionar e/ou remover veículos e acompanha o total de cada um deles:

<p align="center"> 
<img src="https://i.imgur.com/1skIIl4.jpg">
</p>

O terceiro botão o levará a listagem dos fretes realizados:

<p align="center"> 
<img src="https://i.imgur.com/Ai47qQB.jpg">
</p>

Nesta tela mostra todos os fretes realizados com seus respectivos dados:

<p align="center"> 
<img src="https://i.imgur.com/Lhknmg3.jpg">
</p>

No quarto botão, o extrato bancário é apresentado:

<p align="center"> 
<img src="https://i.imgur.com/PJk5TqZ.jpg">
</p>

Você terá acesso aos valores dos fretes realizados e seu valor total arrecadado:

<p align="center"> 
<img src="https://i.imgur.com/RtGX4FL.jpg">
</p>

No quinto e último botão, é a escolha do lucro para os cálculos:

<p align="center"> 
<img src="https://i.imgur.com/4zfjSYH.jpg">
</p>

Ao clicar abrirá uma janela onde você digitara o valor, em porcentágem, do lucro dos fretes:

<p align="center"> 
<img src="https://i.imgur.com/NIe0vIM.jpg">
</p>

## Observações

- OpenJDK version: "1.8.0-212"
- IDE: Eclipse 4.11.0

